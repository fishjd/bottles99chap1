import junit.framework.TestCase;

public class Bottle99Test extends TestCase {

	Bottle99 bottle99;

	public void setUp() throws Exception {
		super.setUp();
		bottle99 = new Bottle99();
	}

	public void tearDown() throws Exception {
	}

	public void testFirstVerse() {
		String result = bottle99.verse(99);

		String expected = "99 bottles of beer on the wall, " + "99 bottles of beer.\n"
			+ "Take one down and pass it around, " + "98 bottles of beer on the wall.\n";
		assertEquals(expected, result);
	}

	public void test_another_verse() {
		String expected = "3 bottles of beer on the wall, " + "3 bottles of beer.\n"
			+ "Take one down and pass it around, " + "2 bottles of beer on the wall.\n";
		assertEquals(expected, bottle99.verse(3));
	}


	public void test_verse_1 () {
	String 	expected = "1 bottle of beer on the wall, " + "1 bottle of beer.\n"
			+ "Take it down and pass it around, " + "no more bottles of beer on the wall.\n";
		assertEquals( expected, bottle99.verse(1));
	}
}