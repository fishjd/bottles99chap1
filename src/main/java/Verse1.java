public class Verse1 implements IVerse {

	protected Integer number;

	public Verse1() {

	}

	public Verse1(Integer number) {
		this.number = number;
	}

	public String verse() {
		String result =                                     //
			"1 bottle of beer on the wall, "                //
				+ "1 bottle of beer.\n"                     //
				+ "Take it down and pass it around, "       //
				+ "no more bottles of beer on the wall.\n"; //
		return result;
	}

}
