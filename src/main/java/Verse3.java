public class Verse3 implements IVerse {

	protected Integer number;

	public Verse3() {

	}

	public Verse3(Integer number) {
		this.number = number;
	}

	public String verse() {
		String result =                                             //
			"3 bottles of beer on the wall, " +                     //
				"3 bottles of beer.\n" +                            //
				"Take one down and pass it around, " +              //
				"2 bottles of beer on the wall.\n";                 //
		return result;
	}

}
