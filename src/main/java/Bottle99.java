public class Bottle99 {

	public static void main(String[] args) {

	}

	public String verse(Integer number) {
		IVerse verse = factory(number);
		String result = verse.verse();
		return result;
	}

	public static IVerse factory(Integer number) {
		IVerse result = null;
		switch (number) {
			case 1:
				result = new Verse1();
				break;
			case 3:
				result = new Verse3();
				break;
			default:
				result = new VerseGeneral(number);
		}
		return result;
	}

}
