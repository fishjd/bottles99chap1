public class VerseGeneral implements IVerse {

	protected Integer number;

	public VerseGeneral(Integer number) {
		this.number = number;
	}

	public String verse() {
		String result =                                             //
			number.toString() + " bottles of beer on the wall, "    //
				+ number + " bottles of beer.\n"                    //
				+ "Take one down and pass it around, "              //
				+ (number - 1) + " bottles of beer on the wall.\n"; //

		return result;
	}

}
